# UOC – Máster en diseño y desarrollo de Videojuegos 2018
# Jaume Castells Carles
# PEC 3 - Un juego de artillería
# Fork de PEC 2 - Un juego de aventuras

## Nombre
**Mario Bros. Clone**

## Descripción
Clone del videojuego **Mario Bros.** (Nintendo)

## Arte
Todo el arte es propiedad de Nintendo.

## Mejoras y fixes en esta PEC 3
*  Sound manager arreglado. Busca automáticamente un canal libre para emitir el sonido deseado sin cortar otras emisiones. El número de audio sources debe ser mayor que 1 debido a que la música siempre ocupa un canal.
*  Incluídas animaciones pendientes de Mario.
*  Todos los efectos de sonido que faltaban ya están incluídos (disparo, powerup, muerte de mario, game over, stage clear, ...).
*  Enemigos Koopa totalmente funcionales.
*  Power up flor incluído.
*  Disparo funcional con una pool de fireballs para ahorrar tiempos de instanciado.
*  Power up estrella incluído.
*  Animación Mario bajando flag pole (también sonido) incluído.
*  Muerte de enemigos por disparo incluído.
*  Muerte de enemigos por invulnerabilidad de Mario debido a power up estrella incluído.
*  Animación bandera de castillo al acabar pantalla incluído.
*  Sistema de partículas para efectos en recogida de power ups y transformaciones.
*  Transformaciones de Mario a través de sprites y shaders.
*  Sistema de input con compilación condicional.

## Para mejorar en futuras versiones
Hay muchos detalles que no he tenido tiempo de añadir y/o arreglar. Espero hacerlo en futuras versiones:  :-)
* Bajada de bandera al animar a Mario en la flag pole.
* Acabar sistema para matar enemigos encima de bloques. Añadido pero desactivado pendiente de arreglar (por trigger enemigo de proximidad).
* Añadir pantallas de tuberías.
* Preparar para ir añadiendo pantallas tan sólo creándolas (para level designers y artistas).
* Modificar disparo fireball por sistema de partículas.
* Ampliación de compilación condicional para Android (controles en UI).

## Builds
Builds available:
### Android
Default target
### Windows
Path Builds/Win
### Linux
### Mac

## Programación
El objeto principal es el **GameManager**. Se encarga de crear y mantener la partida del jugador (estadísticas y info), cambiar de nivel, y de crear el manager de sonido además de otros cometidos generales.

Además del manager de juego también existen otros con sus propios cometidos:
* **StageManager**; se encarga de crear y mantener la pantalla actual. Está pensado para usarse de modo local en cada escena (nivel) con sus propiedades. Hay tantos StageManager como niveles.
* **SoundManager**; encargado del sonido. Sólo hay uno por partida ya que todo el tema sonido está centralizado en él. Contiene varias fuentes de sonido que se encarga de administrar.
* **UIManager**; encargado de la interfaz de usuario. Cuida tanto de los menús como del hud del jugador.

### Otras clases importantes
#### PlayerBehavior
Clase encargada del funcionamiento del player humano.

#### CameraBehavior
Clase base abstracta para el desarrollo de las camaras de juego.

De momento sólo hay una, la SMWCamera (Super Mario World cam).

#### SMWCamera
Sistema de cámara con las peculiaridades y el comportamiento de la cámara del videojuego para NES Mario Bros.

#### InteractibleBlock
Clase base para el comportamiento de los "blocks" interactivos (con sorpresa o no en forma de bonus en el interior).

#### QuestionBlock
Clase heredada de InteractibleBlock para los blocks interrogante (?).

**Mucha más información en el código de los scripts en forma de comentarios y sumarios.**

 
