﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NextGenSprites;

/// <summary>
/// Human player behavior
/// </summary>
public class PlayerBehavior : MonoBehaviour
{
    // Two colliders (one for usual state and one for "super" state)
    public Collider2D marioCollider;
    public Collider2D superMarioCollider;

    // For disabling damaging events pause.
    public float noDamageTime = 2.0f;

    // For disabling damaging on star event.
    public float starInvulnerableTime = 10.0f;
    public Vector4 normalHSBC;
    private Vector4 currentHSBC;
    public float hsbcChangeTime = 1.0f;

    // Whispers effect
    public GameObject whisperEffectPrefab;
    private ParticleSystem whisperEffect;
    public Color starPickedEffectColor;
    public Color mushroomPickedEffectColor;
    public Color flowerPickedEffectColor;

    // Fireballs
    public GameObject fireballSpawnPrefab;
    public Vector4 marioGunnerHSBC;
    private GameObject fireballSpawnObject;
    //private FireballSpawn fireballSpawn;

    // For mario dead animation time purposes.
    public float marioDeathMotionTime = 2.0f;
    public float marioDeathMotionVelocityMultiplier = 2.0f;

    // For player motion customization.
    public float playerVelocityMultiplier;
    public float playerJumpingMultiplier;

    // All "ground" layers mask.
    public LayerMask groundMask;

    public LayerMask destructableBlocksMask;

    public enum MotionState { Idle, Walking, Jumping, Falling, Paused, Dead, Descending}
    public MotionState motionState;

    private bool lookingLeft; // Player's 2D direction

    private float distToGround; // Player's distance to ground

    /// <summary>
    /// "Super" state. Pending to convert to a more protected type.
    /// </summary>
    public bool superMario;

    private bool marioGunner;

    // Is Mario touching the ground?
    private bool onGround;

    private Rigidbody2D playerRigidbody;

    private Animator playerAnimator;

    private SpriteRenderer playerRenderer;
    private Material playerMaterial;

    private Collider2D playerCollider;

    private FireballSpawn playerGun;

    private float horizontalAxis;
    private bool jumpButton;
    private bool fireButton;

    private RaycastHit2D[] headHits;

    private bool noDamagePause = false;

    /// <summary>
    /// To avoid more than one interactable block touch
    /// </summary>
    private bool oneObjectHeaded;

    // Use this for initialization
    void Start ()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        playerAnimator = GetComponent<Animator>();
        playerRenderer = GetComponent<SpriteRenderer>();
        playerMaterial = playerRenderer.sharedMaterial;
        playerCollider = GetComponent<Collider2D>();

        CreateFireballSpawn();
        CreateWhisperEffect();

        InitPlayerProps();
	}
	
    void Animate ()
    {
        playerAnimator.SetFloat("velocity", Mathf.Abs(playerRigidbody.velocity.x));
        playerAnimator.SetBool("jumping", motionState == MotionState.Jumping);
        playerAnimator.SetBool("super", superMario);
        playerAnimator.SetBool("dead", motionState == MotionState.Dead);
        playerAnimator.SetBool("descending", motionState == MotionState.Descending);
        playerRenderer.flipX = lookingLeft;
    }

    void CreateFireballSpawn ()
    {
        fireballSpawnObject = Instantiate(fireballSpawnPrefab, transform);
        fireballSpawnObject.transform.localPosition = new Vector3(0.1f, 0.1f, 0);
        //fireballSpawn = fireballSpawnObject.GetComponent<FireballSpawn>();
    }

    void CreateWhisperEffect ()
    {
        GameObject whisperEffectObj = Instantiate(whisperEffectPrefab, transform);
        whisperEffectObj.transform.localPosition = Vector3.zero;

        whisperEffect = whisperEffectObj.GetComponent<ParticleSystem>();
    }

    /// <summary>
    /// If in "Super" state then disable it and start a no damage pause to avoiding almost same time damage events.
    /// </summary>
    void DamagePlayer ()
    {
        GlobalRefs.currentSoundManager.PlaySound(SoundManager.Sounds.Damage);

        if (superMario)
        {
            StartCoroutine(NoDamagePauseTimer(noDamageTime));
            SetSuperMario(false);
            if (marioGunner)
            {
                SetMarioGunner(false);
            }
        }
        else
        {
            DeadPlayer();
        }
    }

    /// <summary>
    /// Deactivate physics and start player dead animation.
    /// </summary>
    public void DeadPlayer ()
    {
        GlobalRefs.currentGameManager.player.currentLives--;
        GlobalRefs.currentSoundManager.PauseMusic(true);
        PausePlayer(true);
        playerRigidbody.velocity = Vector2.zero;
        playerRigidbody.bodyType = RigidbodyType2D.Kinematic;
        GlobalRefs.currentStageManager.PlayerDead();
        SetMotionState(MotionState.Dead);

        StartCoroutine(DeadMotion());
    }

    /// <summary>
    /// Up and down motion animation at player dead time.
    /// </summary>
    /// <returns></returns>
    IEnumerator DeadMotion()
    {
        float startTime = Time.time;

        DisablePlayerColliders();
        playerRigidbody.freezeRotation = false;

        while (Time.time < (startTime + marioDeathMotionTime))
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + (marioDeathMotionVelocityMultiplier * Time.deltaTime), transform.position.z);
            yield return new WaitForEndOfFrame();
        }

        startTime = Time.time;

        while (Time.time < (startTime + marioDeathMotionTime))
        {
            transform.position = new Vector3(transform.position.x, transform.position.y - (marioDeathMotionVelocityMultiplier * 2 * Time.deltaTime), transform.position.z);
            yield return new WaitForEndOfFrame();
        }

        if (GlobalRefs.currentGameManager.player.currentLives < 1)
        {
            GlobalRefs.currentStageManager.SetMatchState(StageManager.MatchState.Lost);
        }
        else
        {
            GlobalRefs.currentStageManager.SetMatchState(StageManager.MatchState.Reseting);
        }

        playerRigidbody.freezeRotation = true;
    }

    /// <summary>
    /// Picking ribbon.
    /// </summary>
    /// <returns></returns>
    IEnumerator Descending (Vector3 startPosition, Vector3 endPosition)
    {
        float descendingVelocity = 2.0f;
        float yPosition = transform.position.y;

        while (transform.position.y > endPosition.y)
        {
            yPosition -= (descendingVelocity * Time.deltaTime);
            transform.position = new Vector3(transform.position.x, yPosition, transform.position.z);

            yield return new WaitForEndOfFrame();
        }

        // Reactivate
        // TODO: UGLY HACK, PENDING TO FIX
        playerRigidbody.bodyType = RigidbodyType2D.Dynamic;
        SetMotionState(MotionState.Paused);
        playerAnimator.SetBool("jumping", true);
        Jump(0.5f, 5);
        yield return new WaitForSeconds(0.5f);
        SetMotionState(MotionState.Idle);
    }

    void DisablePlayerColliders ()
    {
        marioCollider.enabled = false;
        superMarioCollider.enabled = false;
    }

    /// <summary>
    /// Multiplatform target. Android version could be a good idea.
    /// </summary>
    void GetInput () // Conditional compilation for the input method
    {
#if UNITY_EDITOR
        horizontalAxis = Input.GetAxis("Horizontal");
        jumpButton = Input.GetButtonDown("Jump");
        fireButton = Input.GetButtonDown("Fire1");

#elif UNITY_STANDALONE_WIN
        Debug.Log("Unity iPhone");

#elif UNITY_ANDROID
        Debug.Log("Unity iPhone");

#else
        Debug.Log("Any other platform");
#endif

    }

    /// <summary>
    /// Player arrives to the end of the stage.
    /// </summary>
    public void GoalAchieved ()
    {
        ShowPlayer(false);
        SetMotionState(MotionState.Paused);
        StartCoroutine(GlobalRefs.currentStageManager.StageCleared());
    }

    void InitPlayerProps ()
    {
        SetMarioCollider(true);

        onGround = false;

        SetMotionState(MotionState.Idle);

        distToGround = playerCollider.bounds.extents.y;

        ResetHSBC();

        SetMarioGunner(false);

        playerGun = transform.GetChild(0).GetComponent<FireballSpawn>();
    }

    bool IsGrounded()
    {
        return Physics2D.Raycast(transform.position, -Vector2.up, distToGround + 0.01f, groundMask);
    }

    public bool IsLookingLeft ()
    {
        return lookingLeft;
    }

    void Jump ()
    {
        PlayJumpSound();
        playerRigidbody.AddForce(new Vector2(0f, playerJumpingMultiplier), ForceMode2D.Impulse);
    }

    /// <summary>
    /// Alternative jump method for manual calls.
    /// </summary>
    /// <param name="jumpForce"></param>
    void Jump (float jumpForce)
    {
        playerRigidbody.AddForce(new Vector2(0f, playerJumpingMultiplier * jumpForce), ForceMode2D.Impulse);
    }

    void Jump(float jumpForce, float forward)
    {
        playerRigidbody.AddForce(new Vector2(forward, playerJumpingMultiplier * jumpForce), ForceMode2D.Impulse);
    }

    /// <summary>
    /// It manages the player motion and state machine.
    /// </summary>
    void Move ()
    {
        playerRigidbody.velocity = new Vector3(horizontalAxis * playerVelocityMultiplier, playerRigidbody.velocity.y, 0f);

        if (onGround)
        {
            if (playerRigidbody.velocity.x > 0f)
            {
                lookingLeft = false;
                SetMotionState(MotionState.Walking);
            }
            else if (playerRigidbody.velocity.x < 0f)
            {
                lookingLeft = true;
                SetMotionState(MotionState.Walking);
            }
            else
            {
                 SetMotionState(MotionState.Idle);
            }

            if (jumpButton)
            {
                Jump();
            }
        }
        else
        {
            SetMotionState(MotionState.Jumping);
        }

        SetGunPosition();
    }

    /// <summary>
    /// Centralized game objects interactuation to avoid sensors coding dispersion and a better optimization. ("thinking too much" time) 
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter2D (Collision2D collision)
    {
        if ((motionState != MotionState.Paused) && (motionState != MotionState.Dead))
        {
            switch (collision.gameObject.layer)
            {
                case 13: // Interactible blocks
                    if (!oneObjectHeaded) /// To avoid more than one interactable block touch
                    {
                        if (collision.transform.position.y > transform.position.y)
                        {
                            oneObjectHeaded = true;
                            collision.gameObject.SendMessage("Touched", SendMessageOptions.RequireReceiver);
                        }
                    }
                    break;
                case 14: // Items
                    // TODO : hashable tag?
                    switch (collision.gameObject.tag)
                    {
                        case "Mushroom":
                            collision.gameObject.SendMessage("EndMushroom", SendMessageOptions.RequireReceiver);
                            ShowWhisperEffect(mushroomPickedEffectColor);

                            if (superMario)
                            {
                                // TODO : pending to add other mario states and their behaviors
                                GlobalRefs.currentStageManager.AchieveReMushroom(collision.transform.position);
                            }
                            else
                            {
                                GlobalRefs.currentStageManager.AchieveMushroom(transform.position);
                                SetSuperMario(true);
                            }
                            break;
                        case "Flower":
                            collision.gameObject.SendMessage("EndFlower", SendMessageOptions.RequireReceiver);
                            GlobalRefs.currentStageManager.AchieveFlower(collision.transform.position);
                            ShowWhisperEffect(flowerPickedEffectColor);
                            SetMarioGunner(true);
                            break;
                        case "Star":
                            collision.gameObject.SendMessage("EndStar", SendMessageOptions.RequireReceiver);
                            GlobalRefs.currentStageManager.AchieveStar(collision.transform.position);
                            ShowWhisperEffect(starPickedEffectColor);
                            SetMarioGod();
                            break;
                    }
                    break;
                case 17: // Enemies
                    if (transform.position.y > collision.collider.bounds.center.y)
                    {
                        collision.gameObject.SendMessage("DamageEnemy", noDamagePause, SendMessageOptions.RequireReceiver);
                        Jump(0.5f);
                    }
                    else
                    {
                        if (!noDamagePause)
                        {
                            DamagePlayer();
                        }
                        else
                        {
                            collision.gameObject.SendMessage("DamageEnemy", noDamagePause, SendMessageOptions.RequireReceiver);
                        }
                    }
                    break;
            }
        }
    }

    /// <summary>
    /// Centralize all area triggers inside player behavior
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Ribbon") // Near end of level
        {
            if (!GlobalRefs.currentStageManager.GetStageRibbonState())
            {
                GlobalRefs.currentStageManager.SetHoistStageRibbon(true);

                if (motionState != MotionState.Descending)
                {
                    Vector3 ribbonStartPosition = collision.transform.GetChild(0).position;
                    Vector3 ribbonEndPosition = collision.transform.GetChild(1).position;

                    RibbonEvent(ribbonStartPosition, ribbonEndPosition);
                }
            }
        }
    }

    /// <summary>
    /// Pause moment for player post-damaged times. Avoids re-damage.
    /// </summary>
    /// <returns></returns>
    IEnumerator NoDamagePauseTimer (float time)
    {
        noDamagePause = true;
        yield return new WaitForSeconds(time);

        noDamagePause = false;
    }

    void PausePlayer (bool pause)
    {
        if (pause)
        {
            SetMotionState(MotionState.Paused);
        }
        else
        {
            SetMotionState(MotionState.Idle);
        }
    }

    void PlayJumpSound ()
    {
        GlobalRefs.currentSoundManager.PlaySound(SoundManager.Sounds.Jump);
    }

    private void ResetHSBC ()
    {
        playerMaterial.SetVector(ShaderVector4.HSBC.GetString(), normalHSBC);
    }

    /// <summary>
    /// Reset player position when player deads.
    /// </summary>
    public void ResetPlayer ()
    {
        transform.position = GlobalRefs.currentStageManager.spawnPoints[0].transform.position;
        PausePlayer(false);
        playerRigidbody.velocity = Vector2.zero;
        marioCollider.enabled = true;
        playerRigidbody.bodyType = RigidbodyType2D.Dynamic;
    }

    private void RibbonEvent (Vector3 startPosition, Vector3 endPosition)
    {
        // Deactivate interactuation and physics
        PausePlayer(true);
        playerRigidbody.bodyType = RigidbodyType2D.Kinematic;
        playerRigidbody.velocity = Vector3.zero;

        // Engage to item
        transform.position = new Vector3(startPosition.x, transform.position.y, transform.position.z);

        // Animate and move
        SetMotionState(MotionState.Descending);
        GlobalRefs.currentSoundManager.PlaySound(SoundManager.Sounds.FlagPole);
        StartCoroutine(Descending(startPosition, endPosition));

    }

    private void SetGunPosition ()
    {
        if (lookingLeft)
        {
            fireballSpawnObject.transform.localPosition = new Vector3(-0.1f, 0.1f, 0f);
        }
        else
        {
            fireballSpawnObject.transform.localPosition = new Vector3(0.1f, 0.1f, 0f);
        }
    }

    /// <summary>
    /// Toggle between usual mario and "super" mario colliders.
    /// </summary>
    /// <param name="marioCol"></param>
    void SetMarioCollider (bool marioCol)
    {
        marioCollider.enabled = marioCol;
        superMarioCollider.enabled = !marioCol;
    }

    /// <summary>
    /// Star taken. Mario 10 seconds invulnerable.
    /// </summary>
    void SetMarioGod ()
    {
        StartCoroutine(NoDamagePauseTimer(starInvulnerableTime));
        StartCoroutine(ShowMarioStarEffect(starInvulnerableTime));
    }

    /// <summary>
    /// Flower taken. Mario takes fireball power.
    /// </summary>
    void SetMarioGunner (bool gunner)
    {
        marioGunner = gunner;

        if (marioGunner)
        {
            playerMaterial.SetVector(ShaderVector4.HSBC.GetString(), marioGunnerHSBC);
        }
        else
        {
            playerMaterial.SetVector(ShaderVector4.HSBC.GetString(), normalHSBC);
        }
    }

    /// <summary>
    /// Player animation and state machine
    /// </summary>
    /// <param name="ms"></param>
    void SetMotionState (MotionState ms)
    {
        motionState = ms;
        Animate();
    }

    /// <summary>
    /// Set "super" state. Pending to change superMario property protection
    /// </summary>
    /// <param name="setIt"></param>
    public void SetSuperMario (bool setIt)
    {
        superMario = setIt;
        playerAnimator.SetBool("super", setIt);
        SetMarioCollider(!setIt);
    }

    private void Shoot ()
    {
        if (marioGunner)
        {
            playerGun.Shoot();
        }
    }

    IEnumerator ShowMarioStarEffect (float time)
    {
        float initialBright = 0.5f;
        float finalBright = 0.99f;

        float startTime = Time.time;

        currentHSBC = normalHSBC;

        while (Time.time < (startTime + time))
        {
            while (currentHSBC.z < finalBright)
            {
                currentHSBC.z += (hsbcChangeTime * Time.deltaTime);
                playerMaterial.SetVector(ShaderVector4.HSBC.GetString(), currentHSBC);
                yield return new WaitForEndOfFrame();
            }

            while (currentHSBC.z > initialBright)
            {
                currentHSBC.z -= (hsbcChangeTime * Time.deltaTime);
                playerMaterial.SetVector(ShaderVector4.HSBC.GetString(), currentHSBC);
                yield return new WaitForEndOfFrame();
            }
        }

        ResetHSBC();
    }

    private void ShowPlayer (bool show)
    {
        playerRenderer.enabled = show;
    }

    private void ShowWhisperEffect (Color color)
    {
        ParticleSystem.MainModule main = whisperEffect.main;
        main.startColor = color;
        whisperEffect.Play();
    }

    private void FixedUpdate()
    {
        onGround = IsGrounded();

        /// To avoid more than one interactable block touch
        if (onGround && oneObjectHeaded)
        {
            oneObjectHeaded = false;
        }
    }

    void Update ()
    {
        GetInput();

        if ((motionState != MotionState.Paused) && (motionState != MotionState.Dead) && (motionState != MotionState.Descending))
        {
            Move();

            if (fireButton)
            {
                Shoot();
            }
        }

    }
}
