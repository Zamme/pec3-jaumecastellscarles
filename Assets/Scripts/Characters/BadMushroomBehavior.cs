﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Goombas enemy class
/// </summary>
public class BadMushroomBehavior : EnemyBehavior
{
    protected override void Start()
    {
        base.Start();

        speed = 1.0f;

        // Set bonus type
        bonusType = ScoreBonus.ScoreBonusType.MushroomKill;
    }

    /// <summary>
    /// Basic 2D chassing player system
    /// </summary>
    void ChasePlayer()
    {
        if (playerTransform.position.x < transform.position.x)
        {
            // Go left
            enemyRigidbody.velocity = new Vector2(-speed, enemyRigidbody.velocity.y);
        }
        else
        {
            // Go Right
            enemyRigidbody.velocity = new Vector2(speed, enemyRigidbody.velocity.y);
        }
    }

    protected override void UsualBehave()
    {
        base.UsualBehave();

        ChasePlayer();
    }
}
