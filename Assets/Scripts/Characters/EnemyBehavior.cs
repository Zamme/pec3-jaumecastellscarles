﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Enemy master class. All types of enemies inherit from this.
/// </summary>
public class EnemyBehavior : MonoBehaviour
{
    public enum DificultyLevel { Easy, Medium, Hard}
    public DificultyLevel dificultyLevel = DificultyLevel.Easy;

    public float speed = 1.0f;
    public int lives = 1;
    public float deathTimePause = 3.0f;
    public float reboundImpulse = 5.0f;
    
    /// <summary>
    /// Enemy radar
    /// </summary>
    public Collider2D chassingTrigger;

    public Collider2D enemyCollider;

    public enum EnemyState { Enabled, Disabled, Dead, Paused}
    public EnemyState enemyState;

    private int scoreEffectIndex;

    private int dificultyProbability;

    protected Rigidbody2D enemyRigidbody;

    protected Transform playerTransform;

    protected ScoreBonus.ScoreBonusType bonusType;

    protected virtual void Start()
    {
        enemyRigidbody = GetComponent<Rigidbody2D>();

        // All enemies disabled until player enters at radar distance
        EnableEnemy(false);
    }

    /// <summary>
    /// Enemy state machine
    /// </summary>
    private void CheckState ()
    {
        switch (enemyState)
        {
            case EnemyState.Enabled:
                UsualBehave();
                break;
            case EnemyState.Disabled:
                DisableEnemy();
                break;
            case EnemyState.Dead:
                gameObject.SetActive(false);
                break;
            case EnemyState.Paused:
                DisableEnemy();
                break;
        }
    }

    /// <summary>
    /// Enemy damaged. Prepared for more than one strike possible cases.
    /// </summary>
    public void DamageEnemy (bool deadByShot)
    {
        lives--;

        if (lives < 1)
        {
            KillEnemy(deadByShot);
        }
    }

    /// <summary>
    /// Enemy death with a deformation and a pause.
    /// </summary>
    /// <returns></returns>
    IEnumerator DeadByTrample ()
    {
        transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y * 0.5f, transform.localScale.z);
        yield return new WaitForSeconds(deathTimePause);

        SetEnemyState(EnemyState.Dead);
    }

    /// <summary>
    /// Enemy death with a rebound.
    /// </summary>
    /// <returns></returns>
    IEnumerator DeadByShot()
    {
        GlobalRefs.currentSoundManager.PlaySound(SoundManager.Sounds.KickEnemy);
        enemyRigidbody.constraints = RigidbodyConstraints2D.None;
        enemyRigidbody.AddForce(new Vector2(transform.position.x - playerTransform.position.x, 1.0f) * reboundImpulse, ForceMode2D.Impulse);
        enemyRigidbody.AddTorque(-reboundImpulse, ForceMode2D.Impulse);
        yield return new WaitForSeconds(deathTimePause);

        SetEnemyState(EnemyState.Dead);
    }

    private void DisableEnemy ()
    {
        enemyRigidbody.velocity = Vector2.zero;
    }

    public void EnableEnemy (bool enable)
    {
        if (enable)
        {
            SetEnemyState(EnemyState.Enabled);
        }
        else
        {
            SetEnemyState(EnemyState.Disabled);
        }
    }

    private void EnableChassingTrigger (bool enable)
    {
        chassingTrigger.enabled = enable;
    }

    /// <summary>
    /// Kill enemy procedure; disable collider, disable possible motion, report bonus to stage manager, and death animation
    /// </summary>
    protected virtual void KillEnemy (bool deadByShot)
    {
        enemyCollider.enabled = false;
        EnableEnemy(false);
        //enemyRigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
        GlobalRefs.currentStageManager.ReportBonus(bonusType, transform.position);

        if (deadByShot)
        {
            StartCoroutine(DeadByShot());
        }
        else
        {
            StartCoroutine(DeadByTrample());
        }
    }

    /// <summary>
    /// Touched by projectile.
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter2D (Collision2D collision)
    {
        if (collision.gameObject.tag == "Fireball")
        {
            DamageEnemy(true);
            collision.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Detects player approach and enables enemy behavior.
    /// </summary>
    /// <param name="col"></param>
    private void OnTriggerEnter2D (Collider2D col)
    {
        switch (col.tag)
        {
            case "Player":
                // EnableChassingTrigger(false); TODO: PENDING TO FIX!!!
                if (enemyState == EnemyState.Disabled)
                {
                    if (!playerTransform)
                    {
                        playerTransform = col.transform;
                    }

                    EnableEnemy(true);
                }
                break;
            case "Fireball":
                break;
            default:
                break;
        }

    }
    
    /// <summary>
    /// Enemy behavior is disabled when player moves away from
    /// </summary>
    /// <param name="col"></param>
    private void OnTriggerExit2D (Collider2D col)
    {
        if (col.tag == "Player")
        {
            // EnableChassingTrigger(true); TODO: PENDING TO FIX!!!
            if (enemyState == EnemyState.Enabled)
            {
                EnableEnemy(false);
            }
         }
    }
    
    public void PauseEnemy ()
    {
        SetEnemyState(EnemyState.Paused);
    }

    public void ResumeEnemy ()
    {
        SetEnemyState(EnemyState.Disabled);
    }

    /// <summary>
    /// Enemy state machine setter.
    /// </summary>
    /// <param name="state"></param>
    private void SetEnemyState (EnemyState state)
    {
        enemyState = state;
        CheckState();
    }

    /// <summary>
    /// Basic difficulty level manager for enemies.
    /// </summary>
    private void SetDificultyProbability()
    {
        // Random probability
        switch (dificultyLevel)
        {
            case DificultyLevel.Easy:
                dificultyProbability = 25;
                break;
            case DificultyLevel.Medium:
                dificultyProbability = 50;
                break;
            case DificultyLevel.Hard:
                dificultyProbability = 75;
                break;
        }

    }

    /// <summary>
    /// Usual enemy behave. It just chases player.
    /// </summary>
    protected virtual void UsualBehave ()
    {
        if (Random.Range(0, 100) > dificultyProbability)
        {
            // TODO
            return;
        }
    }

    private void FixedUpdate ()
    {
        if (enemyState == EnemyState.Enabled)
        {
            UsualBehave();
        }
    }
}
