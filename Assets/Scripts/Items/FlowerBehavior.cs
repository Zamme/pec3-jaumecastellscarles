﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Mushroom type bonus
/// </summary>
public class FlowerBehavior : MonoBehaviour
{
    public Vector3 yMovement;
    public float movementSpeed;

    private Vector3 startPos;
    private Vector3 desiredPos1;

    private SpriteRenderer mSpriteRenderer;

    void Start()
    {
        mSpriteRenderer = GetComponent<SpriteRenderer>();

        if (transform.parent)
        {
            transform.position = transform.parent.transform.position;
        }

        startPos = transform.position;

        // Final grow effect position
        desiredPos1 = transform.position + yMovement;

        InitializeFlower();
    }

    public void EndFlower()
    {
        // TODO : effects?
        Destroy(gameObject);
    }

    /// <summary>
    /// It surges from a block (it grows from). Pending to create a general purpouse function to englobe all bonus types.
    /// </summary>
    /// <returns></returns>
    IEnumerator Grow()
    {
        float t = 0f;

        while (transform.position.y < desiredPos1.y)
        {
            transform.position = Vector3.Lerp(startPos, startPos + yMovement, t);
            t += (movementSpeed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }

    }

    private void InitializeFlower()
    {
        mSpriteRenderer.sortingLayerName = "ProtoItems";
        StartCoroutine(Grow());
    }

}
