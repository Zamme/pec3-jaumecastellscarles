﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Mushroom type bonus
/// </summary>
public class StarBehavior : MonoBehaviour
{
    public Vector3 yMovement;
    public float movementSpeed;

    private Vector3 startPos;
    private Vector3 desiredPos1;

    public Vector2 initialDirection;

    private Rigidbody2D mRigidbody;
    private Collider2D mCollider;
    private SpriteRenderer mSpriteRenderer;

    void Start()
    {
        mRigidbody = GetComponent<Rigidbody2D>();
        mCollider = GetComponent<Collider2D>();
        mSpriteRenderer = GetComponent<SpriteRenderer>();

        transform.position = transform.parent.transform.position;
        startPos = transform.position;

        // Final grow effect position
        desiredPos1 = transform.position + yMovement;

        InitializeStar();
    }

    /// <summary>
    /// Pending to change physics behavior to simple animation.
    /// </summary>
    /// <param name="enable"></param>
    private void EnablePhysics(bool enable)
    {
        mCollider.enabled = enable;
        mRigidbody.bodyType = enable ? RigidbodyType2D.Dynamic : RigidbodyType2D.Kinematic;
    }

    public void EndStar()
    {
        // TODO : effects?
        Destroy(gameObject);
    }

    /// <summary>
    /// It surges from a block (it grows from). Pending to create a general purpouse function to englobe all bonus types.
    /// </summary>
    /// <returns></returns>
    IEnumerator Grow()
    {
        float t = 0f;

        while (transform.position.y < desiredPos1.y)
        {
            transform.position = Vector3.Lerp(startPos, startPos + yMovement, t);
            t += (movementSpeed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }

        LeaveItFree();
    }

    private void InitializeStar()
    {
        mSpriteRenderer.sortingLayerName = "ProtoItems";
        EnablePhysics(false);
        StartCoroutine(Grow());
    }

    private void LeaveItFree()
    {
        EnablePhysics(true);
        mRigidbody.AddForce(initialDirection, ForceMode2D.Impulse);
    }

    private void VelocityLimiter ()
    {

    }
}
