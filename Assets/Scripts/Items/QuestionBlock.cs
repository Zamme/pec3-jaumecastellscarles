﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Question type block. Inherits from InteractibleBlock. 
/// </summary>
public class QuestionBlock : InteractibleBlock
{
    public int nBonus = 1;

    // Post-get-bonus empty question block image
    public Sprite endSprite;

    public GameObject mushroomPrefab;
    public GameObject coinPrefab;
    public GameObject flowerPrefab;
    public GameObject starPrefab;

    protected override void Start()
    {
        base.Start();
    }

    protected override void EndBlock()
    {
        base.EndBlock();

        bonus = Bonus.Nothing;

        spriteRenderer.sprite = endSprite;
    }

    /// <summary>
    /// Bonus manager.
    /// </summary>
    /// <returns></returns>
    protected override IEnumerator GetBonus ()
    {
        switch (bonus)
        {
            case Bonus.Nothing:
                break;
            case Bonus.Empty:
                ReportBonus(ScoreBonus.ScoreBonusType.EmptyBlock, transform.position);
                break;
            case Bonus.Mushroom:
                GiveMushroom();
                break;
            case Bonus.Coins:
                GiveCoins();
                ReportBonus(ScoreBonus.ScoreBonusType.Coin, transform.position);
                break;
            case Bonus.Live:
                // TODO
                break;
            case Bonus.Star:
                GiveStar();
                break;
            case Bonus.Flower:
                GiveFlower();
                break;
        }

        yield return new WaitForEndOfFrame();
    }

    /// <summary>
    /// Coin type bonus case.
    /// </summary>
    private void GiveCoin ()
    {
        nBonus--;
        Instantiate(coinPrefab, transform.position + Vector3.up, Quaternion.identity);
    }

    /// <summary>
    /// Multicoin cases.
    /// </summary>
    private void GiveCoins ()
    {
        GiveCoin();

        if (nBonus < 1)
        {
            EndBlock();
        }
    }

    /// <summary>
    /// Mushroom type bonus effect.
    /// </summary>
    private void GiveFlower()
    {
        GlobalRefs.currentSoundManager.PlaySound(SoundManager.Sounds.PowerUpAppears);
        GameObject flower = GameObject.Instantiate(flowerPrefab, transform);
        flower.transform.localScale = Vector3.one;
        flower.transform.position = Vector3.zero;

        EndBlock();
    }

    /// <summary>
    /// Mushroom type bonus effect.
    /// </summary>
    private void GiveMushroom()
    {
        GlobalRefs.currentSoundManager.PlaySound(SoundManager.Sounds.PowerUpAppears);
        GameObject mushroom = GameObject.Instantiate(mushroomPrefab, transform);
        mushroom.transform.localScale = Vector3.one;
        mushroom.transform.position = Vector3.zero;

        EndBlock();
    }

    private void GiveStar()
    {
        GlobalRefs.currentSoundManager.PlaySound(SoundManager.Sounds.PowerUpAppears);
        GameObject star = GameObject.Instantiate(starPrefab, transform);
        star.transform.localScale = Vector3.one;
        star.transform.position = Vector3.zero;

        EndBlock();
    }


}
