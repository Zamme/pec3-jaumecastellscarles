﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: Change it to simple sprite animation.

/// <summary>
/// Coin effect test. Pending to change to simple sprite animation.
/// </summary>
public class CoinBehavior : MonoBehaviour
{
    public int points;
    public Vector3 throwImpulse;

    private Rigidbody2D coinRigidbody;

    private void Start()
    {
        coinRigidbody = GetComponent<Rigidbody2D>();

        LaunchMe();
    }

    private void LaunchMe()
    {
        coinRigidbody.AddForce(throwImpulse, ForceMode2D.Impulse);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Destroy(gameObject);
    }

}
