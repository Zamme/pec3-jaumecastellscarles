﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageManager : MonoBehaviour
{
    public enum MatchState { Preparing, Starting, Playing, Won, Lost, Reseting}
    private MatchState matchState;

    public GameObject uiPrefab;

    public GameObject marioPrefab;
    public int totalMatchTime;
    public string levelName;
    public AudioClip stageMusic;

    public int currentMatchTime;

    public int perMushroomKillBonus = 100;
    public int perKoopaKillBonus = 200;
    public int perReMushroomBonus = 1000;
    public int perMushroomBonus = 1000;
    public int perEmptyBlockBonus = 50;
    public int perCoinBonus = 200;
    public int perFlowerBonus = 100;
    public int perStarBonus = 1000;

    private string currentLevelName;
    private int startTime;
    private int currentTime;
    private bool timerWorking = false;

    public GameObject[] spawnPoints;
    GameObject[] enemies;

    private GameObject player;
    private PlayerBehavior playerBehavior;

    private bool stageRibbonHoisted;
    private GameObject castleRibbon;

    private void Awake()
    {
        ReportReference();
    }

    void Start ()
    {
        SetHoistStageRibbon(false);
        ShowCastleRibbon(false);
    }

    public void AchieveCoin (Vector3 position)
    {
        GlobalRefs.currentSoundManager.PlaySound(SoundManager.Sounds.Coin);

        Instantiate(GlobalRefs.currentGameManager.GetScoreBonusRef().scoreEffects[GlobalRefs.currentGameManager.GetScoreBonusRef().GetScoreEffectIndex(ScoreBonus.ScoreBonusType.Coin)].scoreBonusEffect,
    position, Quaternion.identity);

        AddScoreCoins(1);
        AddScorePoints(perCoinBonus);
    }

    public void AchieveEmptyBlock (Vector3 position)
    {
        GlobalRefs.currentSoundManager.PlaySound(SoundManager.Sounds.BreakBlock);

        Instantiate(GlobalRefs.currentGameManager.GetScoreBonusRef().scoreEffects[GlobalRefs.currentGameManager.GetScoreBonusRef().GetScoreEffectIndex(ScoreBonus.ScoreBonusType.EmptyBlock)].scoreBonusEffect,
            position, Quaternion.identity);

        AddScorePoints(perEmptyBlockBonus);
    }

    public void AchieveFlower(Vector3 position)
    {
        GlobalRefs.currentSoundManager.PlaySound(SoundManager.Sounds.PowerUp);

        Instantiate(GlobalRefs.currentGameManager.GetScoreBonusRef().scoreEffects[GlobalRefs.currentGameManager.GetScoreBonusRef().GetScoreEffectIndex(ScoreBonus.ScoreBonusType.Flower)].scoreBonusEffect,
position, Quaternion.identity);

        AddScorePoints(perFlowerBonus);
    }

    public void AchieveKoopaKill(Vector3 position)
    {
        AddScorePoints(perKoopaKillBonus);
    }

    public void AchieveMushroomKill(Vector3 position)
    {
        GlobalRefs.currentSoundManager.PlaySound(SoundManager.Sounds.Kill);

        Instantiate(GlobalRefs.currentGameManager.GetScoreBonusRef().scoreEffects[GlobalRefs.currentGameManager.GetScoreBonusRef().GetScoreEffectIndex(ScoreBonus.ScoreBonusType.EmptyBlock)].scoreBonusEffect,
            position, Quaternion.identity);

        AddScorePoints(perMushroomKillBonus);
    }

    public void AchieveMushroom(Vector3 position)
    {
        GlobalRefs.currentSoundManager.PlaySound(SoundManager.Sounds.SuperMario);

        Instantiate(GlobalRefs.currentGameManager.GetScoreBonusRef().scoreEffects[GlobalRefs.currentGameManager.GetScoreBonusRef().GetScoreEffectIndex(ScoreBonus.ScoreBonusType.Mushroom)].scoreBonusEffect,
    position, Quaternion.identity);

        AddScorePoints(perMushroomBonus);
    }

    public void AchieveReMushroom(Vector3 position)
    {
        GlobalRefs.currentSoundManager.PlaySound(SoundManager.Sounds.SuperMario);

        Instantiate(GlobalRefs.currentGameManager.GetScoreBonusRef().scoreEffects[GlobalRefs.currentGameManager.GetScoreBonusRef().GetScoreEffectIndex(ScoreBonus.ScoreBonusType.ReMushroom)].scoreBonusEffect,
    position, Quaternion.identity);

        AddScorePoints(perReMushroomBonus);
    }

    public void AchieveStar(Vector3 position)
    {
        GlobalRefs.currentSoundManager.PlaySound(SoundManager.Sounds.PowerUpAppears);

        Instantiate(GlobalRefs.currentGameManager.GetScoreBonusRef().scoreEffects[GlobalRefs.currentGameManager.GetScoreBonusRef().GetScoreEffectIndex(ScoreBonus.ScoreBonusType.Star)].scoreBonusEffect,
    position, Quaternion.identity);

        AddScorePoints(perStarBonus);
    }

    void AddScoreCoins (int coins)
    {
        SetCoins(GlobalRefs.currentGameManager.player.currentStageCoins + coins);
    }

    void AddScorePoints (int points)
    {
        SetScore(GlobalRefs.currentGameManager.player.currentStageScore + points);
    }

    void CheckMatchState ()
    {
        switch (matchState)
        {
            case MatchState.Preparing:
                GetEnemies();
                InitializePlayerStats();
                CreateUI();
                GlobalRefs.currentUIManager.ActivateStartPage();
                StartCoroutine(GlobalRefs.currentUIManager.PauseForSeconds(3));
                break;
            case MatchState.Starting:
                GlobalRefs.currentUIManager.ActivateHud();
                UpdateHUD();
                StartStageMusic();
                CreatePlayer();
                GlobalRefs.currentGameManager.GetCurrentMainCameraBehavior().StartCamera();
                InitializeTimer();
                break;
            case MatchState.Playing:
                break;
            case MatchState.Reseting:
                GlobalRefs.currentUIManager.ActivateStartPage();
                StartCoroutine(GlobalRefs.currentUIManager.PauseForSeconds(3));
                playerBehavior.ResetPlayer();
                GlobalRefs.currentGameManager.GetCurrentMainCameraBehavior().ResetCamera();
                ResumeAll();
                GlobalRefs.currentSoundManager.PauseMusic(false);
                SetMatchState(MatchState.Playing);
                break;
            case MatchState.Won:
                GameOver();
                break;
            case MatchState.Lost:
                StartCoroutine(GameLost());
                break;
        }
    }

    void CreatePlayer()
    {
        GetSpawnPoints();

        if (!player)
        {
            player = GameObject.Instantiate(marioPrefab, spawnPoints[0].transform.position, Quaternion.identity);
            player.name = "Player";
            playerBehavior = player.GetComponent<PlayerBehavior>();
        }
    }

    void CreateUI ()
    {
        Instantiate(uiPrefab, Vector3.zero, Quaternion.identity);
    }

    IEnumerator GameLost ()
    {
        GlobalRefs.currentSoundManager.PauseMusic(true);
        GlobalRefs.currentSoundManager.PlaySound(SoundManager.Sounds.GameOver);
        yield return new WaitForSeconds(2);
        GameOver();
    }

    public void GameOver ()
    {
        // TODO : close stage manager
        GlobalRefs.currentGameManager.GetCurrentMainCameraBehavior().DisableCameraMotion();
        GlobalRefs.currentSoundManager.PauseMusic(true);
        GlobalRefs.currentGameManager.GameOver(matchState == MatchState.Won);
    }

    void GetEnemies ()
    {
        enemies = GameObject.FindGameObjectsWithTag("Enemy");

        foreach (GameObject enemy in enemies)
        {
            enemy.name = "Enemy"; // Unity renames
        }
    }

    public bool GetStageRibbonState ()
    {
        return stageRibbonHoisted;
    }

    public PlayerBehavior GetPlayerBehavior ()
    {
        return playerBehavior;
    }

    void GetSpawnPoints ()
    {
        spawnPoints = GameObject.FindGameObjectsWithTag("SpawnPoint");
    }

    void InitializePlayerStats ()
    {
        GlobalRefs.currentGameManager.player.currentStageCoins = 0;
        GlobalRefs.currentGameManager.player.currentStageScore = 0;
        currentMatchTime = totalMatchTime;
    }

    void InitializeTimer ()
    {
        if (!timerWorking)
        {
            startTime = (int)Time.time;
            StartCoroutine(Timing());
        }
    }

    public void PauseAll ()
    {
        foreach (GameObject enemy in enemies)
        {
            enemy.SendMessage("PauseEnemy", SendMessageOptions.DontRequireReceiver);
        }
    }

    public void PlayerDead ()
    {
        PauseAll();
        GlobalRefs.currentSoundManager.PlaySound(SoundManager.Sounds.MarioDie);
    }

    public void ReportBonus (ScoreBonus.ScoreBonusType bonusType, Vector3 position)
    {
        switch (bonusType)
        {
            case ScoreBonus.ScoreBonusType.EmptyBlock:
                AchieveEmptyBlock(position);
                break;
            case ScoreBonus.ScoreBonusType.Coin:
                AchieveCoin(position);
                break;
            case ScoreBonus.ScoreBonusType.Flower:
                AchieveFlower(position);
                break;
            case ScoreBonus.ScoreBonusType.KoopaKill:
                AchieveKoopaKill(position);
                break;
            case ScoreBonus.ScoreBonusType.MushroomKill:
                AchieveMushroomKill(position);
                break;
            case ScoreBonus.ScoreBonusType.ReMushroom:
                AchieveReMushroom(position);
                break;
        }
    }

    void ReportReference ()
    {
        GlobalRefs.currentStageManager = this;
    }

    public void ResumeAll()
    {
        foreach (GameObject enemy in enemies)
        {
            enemy.SendMessage("ResumeEnemy", SendMessageOptions.DontRequireReceiver);
        }
    }

    void SetCoins (int co)
    {
        GlobalRefs.currentGameManager.player.currentStageCoins = co;
        GlobalRefs.currentUIManager.UpdateUICoins();
    }

    public void SetHoistStageRibbon(bool hoisted)
    {
        stageRibbonHoisted = hoisted;
    }

    void SetLevel ()
    {
        GlobalRefs.currentUIManager.UpdateUILevel();
    }

    void SetLevel (string level) // Title setting manual overriding
    {
        levelName = level;
        GlobalRefs.currentUIManager.UpdateUILevel();
    }

    public void SetMatchState (MatchState ms)
    {
        matchState = ms;

        CheckMatchState();
    }

    void SetScore (int sc)
    {
        GlobalRefs.currentGameManager.player.currentStageScore = sc;
        GlobalRefs.currentUIManager.UpdateUIScore();
    }

    void SetTime ()
    {
        currentMatchTime = totalMatchTime;
        GlobalRefs.currentUIManager.UpdateUITime();
    }

    void ShowCastleRibbon (bool show)
    {
        if (!castleRibbon)
        {
            castleRibbon = GameObject.Find("Castle_Ribbon");
        }
        castleRibbon.SetActive(show);
    }

    public IEnumerator StageCleared ()
    {
        // TODO : recounting score
        ShowCastleRibbon(true);
        GlobalRefs.currentGameManager.GetCurrentMainCameraBehavior().DisableCameraMotion();
        GlobalRefs.currentSoundManager.PauseMusic(true);
        GlobalRefs.currentSoundManager.PlaySound(SoundManager.Sounds.StageClear);

        yield return new WaitForSeconds(7);

        GlobalRefs.currentStageManager.SetMatchState(StageManager.MatchState.Won);
    }

    void StartStageMusic()
    {
        GlobalRefs.currentSoundManager.PlayMusic(stageMusic);
    }

    public void StartStage ()
    {
        SetMatchState(MatchState.Preparing);
    }

    IEnumerator Timing ()
    {
        timerWorking = true;

        SetMatchState(MatchState.Playing);

        while (matchState == MatchState.Playing)
        {
            if (currentMatchTime > 0)
            {
                currentMatchTime = (totalMatchTime - ((int)Time.time - startTime));
            }
            else
            {
                SetMatchState(MatchState.Lost);
            }

            GlobalRefs.currentUIManager.UpdateUITime();

            yield return new WaitForEndOfFrame();
        }

        timerWorking = false;
    }

    void UpdateHUD ()
    {
        GlobalRefs.currentUIManager.UpdateHUD();
    }

}
