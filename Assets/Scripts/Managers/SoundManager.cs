﻿using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public int nAudioSources; // Min = 2 to avoid music overriding

    public AudioClip jumpAudioClip;
    public AudioClip coinAudioClip;
    public AudioClip killAudioClip;
    public AudioClip superMarioAudioClip;
    public AudioClip breakBlockAudioClip;
    public AudioClip bumpAudioClip;
    public AudioClip powerUpAppearsAudioClip;
    public AudioClip gameOver;
    public AudioClip stageClear;
    public AudioClip marioDie;
    public AudioClip kickEnemy;
    public AudioClip flagPole;

    public enum Sounds {Jump, Coin, Kill, SuperMario, BreakBlock, Damage, PowerUpAppears, GameOver, StageClear, MarioDie, KickEnemy, FlagPole, PowerUp}

    private AudioSource[] audioSources;
    private int lastUsed = -1;
    private int lastPlaying;
    private int iMusicAudioSource = -1;

    private void Awake()
    {
        GlobalRefs.currentSoundManager = this;

        InitAudioSources();
    }

    void Start ()
    {
        //DontDestroyOnLoad(gameObject);
	}
	
    AudioSource GetNextAudioSource ()
    {
        lastPlaying = lastUsed;
        GetFreeAudioSourceIndex();

        return audioSources[lastUsed];
    }

    void InitAudioSources ()
    {
        if (nAudioSources < 2)
        {
            Debug.LogError("Audio sources number must be 2 minimum!");
            return;
        }

        audioSources = new AudioSource[nAudioSources];

        for (int a = 0; a < nAudioSources; a++)
        {
            audioSources[a] = gameObject.AddComponent<AudioSource>();
        }
    }

    void GetFreeAudioSourceIndex ()
    {
        GetNextAudioSourceIndex();

        while ((audioSources[lastUsed].isPlaying) || (lastUsed == lastPlaying) || (lastUsed == iMusicAudioSource))
        {
            GetNextAudioSourceIndex();
        }
    }

    void GetNextAudioSourceIndex ()
    {
        lastUsed++;

        if (lastUsed == audioSources.Length)
        {
            lastUsed = 0;
        }

    }

    public void PauseMusic (bool pause)
    {
        if (pause)
        {
            audioSources[iMusicAudioSource].Pause();
        }
        else
        {
            audioSources[iMusicAudioSource].UnPause();
        }
    }

    public int PlayAudioClip (AudioClip audioClip)
    {
        AudioSource audioSource = GetNextAudioSource();
        audioSource.clip = audioClip;
        audioSource.Play();

        return lastUsed;
    }

    public void PlayMusic (AudioClip audioClip)
    {
        if (iMusicAudioSource < 0)
        {
            iMusicAudioSource = PlayAudioClip(audioClip);
            audioSources[iMusicAudioSource].loop = true;
        }
        else
        {
            PauseMusic(false);
        }

    }

    /// <summary>
    /// TODO : Transform it to a Dict!
    /// </summary>
    /// <param name="sound"></param>
    public void PlaySound (Sounds sound)
    {
        switch (sound)
        {
            case Sounds.Coin:
                PlayAudioClip(coinAudioClip);
                break;
            case Sounds.Jump:
                PlayAudioClip(jumpAudioClip);
                break;
            case Sounds.Kill:
                PlayAudioClip(killAudioClip);
                break;
            case Sounds.SuperMario:
                PlayAudioClip(superMarioAudioClip);
                break;
            case Sounds.BreakBlock:
                PlayAudioClip(breakBlockAudioClip);
                break;
            case Sounds.Damage:
                PlayAudioClip(bumpAudioClip);
                break;
            case Sounds.PowerUpAppears:
                PlayAudioClip(powerUpAppearsAudioClip);
                break;
            case Sounds.GameOver:
                PlayAudioClip(gameOver);
                break;
            case Sounds.StageClear:
                PlayAudioClip(stageClear);
                break;
            case Sounds.MarioDie:
                PlayAudioClip(marioDie);
                break;
            case Sounds.KickEnemy:
                PlayAudioClip(kickEnemy);
                break;
            case Sounds.FlagPole:
                PlayAudioClip(flagPole);
                break;
            case Sounds.PowerUp:
                PlayAudioClip(superMarioAudioClip);
                break;
        }
    }
}
