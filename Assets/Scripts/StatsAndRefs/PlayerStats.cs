﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Player info class. It could be static on not multiplayer (and simplified) version.
/// </summary>
public class PlayerStats
{
    // TODO : Make under getters and setters for antihacking (f.e. steam data injection) 
    public int currentGlobalScore;
    public int currentGlobalCoins;

    public int currentStageScore;
    public int currentStageCoins;

    public int currentLives;

    public PlayerStats ()
    {

    }

    public PlayerStats (int globalScore, int globalCoins, int stageScore, int stageCoins, int lives) // For loading previously saved player
    {
        currentGlobalScore = globalScore;
        currentGlobalCoins = globalCoins;
        currentStageScore = stageScore;
        currentStageCoins = stageCoins;
        currentLives = lives;
    }
}
