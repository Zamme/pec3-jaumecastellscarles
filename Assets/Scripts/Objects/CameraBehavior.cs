﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// General camera behaviour class. All camera types inherit from it.
/// </summary>
public abstract class CameraBehavior : MonoBehaviour
{
    protected bool resetingCamera = false;

    public abstract void StartCamera();
    public abstract void ResetCamera();
    public abstract void DisableCameraMotion();

}
