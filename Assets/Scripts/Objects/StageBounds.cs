﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Dead zone surface behavior.
/// </summary>
public class StageBounds : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.SendMessage("DeadPlayer", SendMessageOptions.RequireReceiver);
        }
    }
}
