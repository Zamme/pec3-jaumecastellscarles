﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Score bonus points effect
/// </summary>
public class ItemEffect : MonoBehaviour
{
    public float speed = 1.0f;
    public float showingTime = 2.0f;

	void Start ()
    {
        StartCoroutine(SlideEffect());
	}
	
    IEnumerator SlideEffect ()
    {
        float startTime = Time.time;
        while (Time.time < (startTime + showingTime))
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + (speed * Time.deltaTime), transform.position.z);
            yield return new WaitForEndOfFrame();
        }

        Destroy(gameObject);
    }
}
