﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballSpawn : Weapon
{
    public struct Projectile
    {
        public GameObject projectileObject;
        public Rigidbody2D projectileRigidBody;
        public Collider2D projectileCollider;
    }

    public GameObject fireballPrefab;
    public int poolSize;
    public string projectilesPrefix = "Fireball";
    public float cadencyTime = 1.0f;
    public Vector2 projectileImpulse;
    public float projectileLiveTime = 2.0f;

    /// <summary>
    /// Pool of projectiles for instantiation lost time avoiding.
    /// </summary>
    private Projectile[] projectiles;

    private int iNextProjectile;
    private bool nextProjectileReady;

    // Projectile direction impulse
    private Vector2 currentProjectileImpulse;

    // Player behavior link
    private PlayerBehavior playerBehavior;

	void Start ()
    {
	    if (poolSize > 0)
        {
            CreatePool();
            iNextProjectile = 0;
            SetNextProjectileReady(true);
        }
        else
        {
            Debug.LogError("Projectiles pool size is wrong!");
        }

        playerBehavior = GlobalRefs.currentStageManager.GetPlayerBehavior();
	}
	
    void CreatePool ()
    {
        projectiles = new Projectile[poolSize];

        for (int iProjectile = 0; iProjectile < poolSize; iProjectile++)
        {
            projectiles[iProjectile] = new Projectile();
            projectiles[iProjectile].projectileObject = CreateProjectile(iProjectile);
            projectiles[iProjectile].projectileRigidBody = projectiles[iProjectile].projectileObject.GetComponent<Rigidbody2D>();
            projectiles[iProjectile].projectileCollider = projectiles[iProjectile].projectileObject.GetComponent<Collider2D>();
            DisableProjectile(iProjectile);
        }
    }

    GameObject CreateProjectile (int projectileIndex)
    {
        GameObject projectile = Instantiate(fireballPrefab);
        projectile.name = projectilesPrefix + projectileIndex.ToString();
        projectile.transform.position = transform.position;

        return projectile;
    }

    void DisableProjectile (int iProjectile)
    {
        projectiles[iProjectile].projectileRigidBody.velocity = Vector3.zero;
        projectiles[iProjectile].projectileObject.SetActive(false);
    }

    void EnableProjectile (int iProjectile)
    {
        projectiles[iProjectile].projectileObject.transform.position = transform.position;
        projectiles[iProjectile].projectileObject.SetActive(true);
        GiveImpulse(iProjectile);
        StartCoroutine(SetProjectileLiveTime(iProjectile));
    }

    void GiveImpulse (int iProjectile)
    {
        if (playerBehavior.IsLookingLeft())
        {
            currentProjectileImpulse = new Vector2(projectileImpulse.x * -1, projectileImpulse.y);
        }
        else
        {
            currentProjectileImpulse = projectileImpulse;
        }

        projectiles[iProjectile].projectileRigidBody.AddForce(currentProjectileImpulse, ForceMode2D.Impulse);
    }

    void NextProjectileIndex ()
    {
        iNextProjectile++;

        if (iNextProjectile == poolSize)
        {
            iNextProjectile = 0;
        }
    }

    void SetNextProjectileReady (bool ready)
    {
        nextProjectileReady = ready;
    }

    IEnumerator SetProjectileLiveTime (int iProjectile)
    {
        float bornTime = Time.time;

        while (Time.time < (bornTime + projectileLiveTime))
        {
            yield return new WaitForEndOfFrame();
        }

        DisableProjectile(iProjectile);
    }

    public void Shoot ()
    {
        if (nextProjectileReady)
        {
            GlobalRefs.currentSoundManager.PlaySound(SoundManager.Sounds.KickEnemy);
            StartCoroutine(ShootProjectile());
        }
        else
        {
            // TODO: what happens if projectile is not ready
        }
    }

    IEnumerator ShootProjectile ()
    {
        // Avoid reshoot until cadency time
        SetNextProjectileReady(false);
        float initCadencyTime = Time.time;

        EnableProjectile(iNextProjectile);

        while (Time.time < (initCadencyTime + cadencyTime))
        {
            yield return new WaitForEndOfFrame();
        }

        NextProjectileIndex();
        SetNextProjectileReady(true);
    }
}
